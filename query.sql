Create database QLSV;
Create table DMKHOA (
    MaKH varchar(6),
    TenKhoa varchar(30),
    PRIMARY KEY(MaKH)
);
Create table SINHVIEN (
    MaSV varchar(6),
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int,
    PRIMARY KEY(MaSV)
);

SELECT * FROM SINHVIEN
INNER JOIN DMKHOA ON DMKHOA.MaKH = SINHVIEN.MaKH
WHERE TenKhoa = "Cong nghe thong tin";